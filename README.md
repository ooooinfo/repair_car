# 老司机修车网

#### 项目介绍
老司机修车网是一个定位为辅助4s店员工的网站，
分为<Br/>
1.主页：介绍公司主要业务与预约业务<Br/>
2.功能页：给接待人员，修车工使用的<Br/>
3.后台：给管理员使用<Br/>

#### 软件架构
软件架构说明<Br/>
前端使用layui<Br/>
后端使用springmvc+spring+mybatis<Br/>

#### 使用说明

访问地址：<Br/>
主页   www.516code.cn<Br/>
功能页 function.516code.cn<Br/>
管理页 admin.516code.cn<Br/>

接待页 http://function.516code.cn/ReceptionPage/index.html<Br/>
维修人员页 http://function.516code.cn/repairPage/index.html<Br/>


源码地址：03.编码实现/sourceCode/<Br/>
数据库地址：03.编码实现/DB<Br/>
需求地址：01.项目需求/修车项目需求<Br/>

#### 参与贡献

java46 3组


