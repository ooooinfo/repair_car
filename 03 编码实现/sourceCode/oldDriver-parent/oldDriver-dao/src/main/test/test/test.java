package test;

import com.java46.bean.Admin;
import com.java46.mapper.AdminMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.List;

public class test {
    public static void main(String[] args) {
        //拿session对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = test.class.getClassLoader().getResourceAsStream(resource);
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = sessionFactory.openSession();

        //拿mapper对象
        AdminMapper mapper = session.getMapper(AdminMapper.class);

        List<Admin> admins = mapper.selectAll();
        System.out.println(admins);
    }
}
