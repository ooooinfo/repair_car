package com.java46.mapper;

import com.java46.bean.Personnel;
import java.util.List;

public interface PersonnelMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table personnel
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    int deleteByPrimaryKey(Integer personnelId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table personnel
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    int insert(Personnel record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table personnel
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    Personnel selectByPrimaryKey(Integer personnelId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table personnel
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    List<Personnel> selectAll();

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table personnel
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    int updateByPrimaryKey(Personnel record);

    /**
     * 根据personnel的字段查询，如果那个字段不为空则根据该字段查询
     * @param personnel
     * @return
     */
    Personnel PersonnelLogin(Personnel personnel);
    /*
    * 根据员工Id查询返回List集合
    *
    * */
    List<Personnel> selectByPrimaryListKey(Integer personnelId);


}