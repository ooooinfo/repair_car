package com.java46.bean;

public class Order {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column order.order_id
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    private Integer orderId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column order.order_time
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    private String orderTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column order.car_plate
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    private String carPlate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column order.customer_name
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    private String customerName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column order.customer_phone
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    private String customerPhone;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column order.order_status
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    private Integer orderStatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column order.list_status
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    private Integer listStatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column order.repair_status
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    private Integer repairStatus;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column order.order_id
     *
     * @return the value of order.order_id
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column order.order_id
     *
     * @param orderId the value for order.order_id
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column order.order_time
     *
     * @return the value of order.order_time
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public String getOrderTime() {
        return orderTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column order.order_time
     *
     * @param orderTime the value for order.order_time
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column order.car_plate
     *
     * @return the value of order.car_plate
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public String getCarPlate() {
        return carPlate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column order.car_plate
     *
     * @param carPlate the value for order.car_plate
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public void setCarPlate(String carPlate) {
        this.carPlate = carPlate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column order.customer_name
     *
     * @return the value of order.customer_name
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column order.customer_name
     *
     * @param customerName the value for order.customer_name
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column order.customer_phone
     *
     * @return the value of order.customer_phone
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public String getCustomerPhone() {
        return customerPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column order.customer_phone
     *
     * @param customerPhone the value for order.customer_phone
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column order.order_status
     *
     * @return the value of order.order_status
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public Integer getOrderStatus() {
        return orderStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column order.order_status
     *
     * @param orderStatus the value for order.order_status
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column order.list_status
     *
     * @return the value of order.list_status
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public Integer getListStatus() {
        return listStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column order.list_status
     *
     * @param listStatus the value for order.list_status
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public void setListStatus(Integer listStatus) {
        this.listStatus = listStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column order.repair_status
     *
     * @return the value of order.repair_status
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public Integer getRepairStatus() {
        return repairStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column order.repair_status
     *
     * @param repairStatus the value for order.repair_status
     *
     * @mbggenerated Fri Aug 17 16:18:04 CST 2018
     */
    public void setRepairStatus(Integer repairStatus) {
        this.repairStatus = repairStatus;
    }
}