package com.java46.controller;

import com.java46.bean.JsonBean;
import com.java46.bean.Order;
import com.java46.bean.Product;
import com.java46.exception.MyException;
import com.java46.service.TestService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
//模块标识
@RequestMapping("/test")
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class TestController {
    @Autowired
    private TestService testService;

    /**********************************************
     *
     * 查询所有商品（模版）
     *
     * 请求地址 localhost:8080/test/queryProducts.do
     * @return
     * ********************************************
     */
    @RequestMapping("/queryProducts")
    @ResponseBody
    public JsonBean queryProducts(){

        List<Product> allProduct=null;

        try {
            //执行service的查询方法
            allProduct= testService.queryProducts();

        } catch (MyException e) {
            //失败时输入异常，并封装错误信息，return
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        //成功后自动封装正确数据，return
       return ControllerUtil.setSuccess(allProduct);
    }


    /**********************************************
     *
     * 插入商品（模版）
     *
     * 请求地址 localhost:8080/test/insertProduct.do
     *
     * 需要传入【
     *  productName
     *  productNum
     *  productPrice
     *  productImg
     *  productTypeId
     * 】
     *
     *
     *
     * @return
     * ********************************************
     */
    @RequestMapping("/insertProduct")
    @ResponseBody
    public JsonBean testGetAllPro(Product product){

        List<Product> allProduct=null;
        int count;
        try {
            //执行service的查询方法
            count = testService.insertProduct(product);
        } catch (MyException e) {
            //失败时输入异常，并封装错误信息，return
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        //成功后自动封装正确数据，return
        return ControllerUtil.setSuccess("插入成功");
    }








    /**********************************************
     *
     * 通过状态码查订单
     *
     * 请求地址 localhost:8080/test/queryOrderByStatusCode.do
     *
     * 需要传入【
     *  statusCode
     * 】
     *
     *
     *
     * @return
     * ********************************************
     */
    @RequestMapping("/queryOrderByStatusCode")
    @ResponseBody
    public JsonBean queryOrderByStatusCode(String statusCode){
        List<Order> orders;
        try {
            orders = testService.queryOrderByStatusCode(statusCode);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess(orders);
    }

    /**********************************************
     *
     * 改变订单状态
     *
     * 请求地址 localhost:8080/test/changeStatus.do
     *
     * 需要传入【
     *  orderId
     *  statusCode
     * 】
     *
     *
     *
     * @return
     * ********************************************
     */
    @RequestMapping("/changeStatus")
    @ResponseBody
    public JsonBean changeStatus(Integer orderId, String statusCode){
        try {
            testService.queryOrderByStatusCode(orderId,statusCode);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess("修改成功");
    }







}
