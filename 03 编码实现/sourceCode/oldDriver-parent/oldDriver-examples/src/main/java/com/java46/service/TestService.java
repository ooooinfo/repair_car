package com.java46.service;



import com.java46.bean.Order;
import com.java46.bean.Product;
import com.java46.exception.MyException;
import com.java46.mapper.OrderMapper;
import com.java46.mapper.ProductMapper;
import com.java46.util.ServiceUtil;
import com.java46.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
@Transactional
@Service
public class TestService {
    //从spring得mapper接口
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private OrderMapper orderMapper;

    /************************************
     * 查询所有商品（模版）
     *
     * @return
     * @throws MyException
     * *******************************
     */
    public List<Product> queryProducts() throws MyException {
        //执行查询操作
        List<Product> products = productMapper.selectAll();
        //判断是否有数据，（这里使用的是判断数组为空的util方法）
        ServiceUtil.checkListNull(products,"数据库没有查到数据");

        return products;
    }


    /*********************************
     *
     * 插入商品（模版）
     * 需要传入
     *
     * @param product
     * @return
     * @throws MyException
     * ********************************
     */
    public int insertProduct(Product product) throws MyException {
        ServiceUtil.checkNull(product,"传入的商品不能为空");
        //插入操作
        int count;
        try{
            count = productMapper.insert(product);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("oh! 数据库出现了错误");
        }
        ServiceUtil.checkNull(count,"修改失败");
        return  count;
    }


    /*********************************
     *
     * 通过状态码查询订单集合
     *
     * 需要传入状态码
     * 如110,120
     *
     * @param statusCode
     * @return
     * @throws MyException
     * ********************************
     */

    public List<Order> queryOrderByStatusCode(String statusCode) throws MyException {

        List<Order> list;

        try{
            //通过状态码查询订单直接，需要把code用Util.codeToHash转为hash表再查询
            list=orderMapper.queryOrderByStatusCode(Util.codeToHash(statusCode));
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("oh! 数据库出现了错误");
        }
        ServiceUtil.checkListNull(list,"没有数据");
        return  list;
    }


    /*********************************
     *
     * 将某订单转换为某个状态
     *
     *
     * 需要传入
     * 订单id
     *
     * 状态码
     * 如110,120
     *
     * @param
     * @return
     * @throws MyException
     * ********************************
     */

    public boolean queryOrderByStatusCode(Integer orderId,String statusCode) throws MyException {
        Order order = Util.packageOrder(orderId, statusCode);
        int check;
        try{
            check = orderMapper.updateStatusInOrderid(order);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("oh! 数据库出现了错误");
        }
        if (check==0){
            Util.throwMyexception("修改失败");
        }
        return  true;
    }






}
