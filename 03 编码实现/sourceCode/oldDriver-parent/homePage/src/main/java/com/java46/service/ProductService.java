package com.java46.service;

import com.java46.bean.Product;
import com.java46.exception.MyException;
import com.java46.mapper.ProductMapper;
import com.java46.mapper.ProductTypeMapper;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductService {
    @Autowired
    private ProductMapper productMapper;
    @Transactional(readOnly = true)
//    查询所有产品
    public List<Product> QueryProductAll() throws MyException {
        List<Product> products = productMapper.selectAll();
        List product = ServiceUtil.checkListNull(products, "查询异常");
        return product;
    }
//    根据产品类型查询产品
    @Autowired
    private ProductTypeMapper productTypeMapper;
    @Transactional(readOnly = true)
    public List<Product> TypeQueryProductAll(String PrimaryTypeName) throws MyException {
        int i = productTypeMapper.selectByPrimaryName(PrimaryTypeName);
        int productTypeId = ServiceUtil.checkNull(i, "查询产品类型id失败");
        List<Product> products = productMapper.selectByPrimaryTypeKey(productTypeId);
        return ServiceUtil.checkListNull(products,"根据产品类型id查询产品信息失败");
    }
}
