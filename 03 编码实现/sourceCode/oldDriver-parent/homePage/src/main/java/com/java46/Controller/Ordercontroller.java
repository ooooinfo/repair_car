package com.java46.Controller;

import com.java46.bean.JsonBean;
import com.java46.bean.Order;
import com.java46.exception.MyException;
import com.java46.service.OrderService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/homepage")
@CrossOrigin(origins = "*",maxAge = 3600)
public class Ordercontroller {
    @Autowired
    private OrderService orderService;
    //提交预约订单
    @RequestMapping("/insertOrdercontroller")
    @ResponseBody
    public JsonBean insertOrdercontroller(Order order){
        try {
             orderService.insertOrder(order);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess("添加成功");
    }
}
