package com.java46.service;

import com.java46.bean.LeaveWord;
import com.java46.exception.MyException;
import com.java46.mapper.LeavewordMapper;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class LeavwordService {
    @Autowired
    private LeavewordMapper leavewordMapper;
    @Transactional(readOnly = true)
    //查询全部留言
    public List<LeaveWord> QueryLeaveWordAll() throws MyException {
        List<LeaveWord> leaveWords = leavewordMapper.QueryleavewordAll();
        List leaveword = ServiceUtil.checkListNull(leaveWords, "查询留言失败");
        return leaveword;
    }
    //添加客户留言
    public void InsertLeaveword(LeaveWord leaveWord) throws MyException {
        Integer integer;
        try {
            integer = leavewordMapper.insertLeaveword(leaveWord);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException(e.getMessage());
        }
        ServiceUtil.checkNull(integer, "添加客户留言异常");
    }
}
