package com.java46.service;



import com.java46.bean.CarBrand;
import com.java46.mapper.CarBrandMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CarbrandService {
    @Autowired
    private CarBrandMapper carBrandMapper;

    @Transactional(readOnly = true)
    public List<CarBrand> QueryCar(){
        List<CarBrand> carBrands = carBrandMapper.selectAll();
        return carBrands;
    }
}
