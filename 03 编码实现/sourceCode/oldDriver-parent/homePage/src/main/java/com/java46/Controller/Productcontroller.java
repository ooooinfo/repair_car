package com.java46.Controller;

import com.java46.bean.JsonBean;
import com.java46.bean.Product;
import com.java46.exception.MyException;
import com.java46.service.ProductService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/homepage")
@CrossOrigin(origins = "*",maxAge = 3600)
public class Productcontroller {
    @Autowired
    private ProductService productService;
    //查询所有产品信息
    @RequestMapping("/QueryProductAll")
    @ResponseBody
    public JsonBean QueryProductAll(){
        try {
            List<Product> products = productService.QueryProductAll();
            return ControllerUtil.setSuccess(products);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
    }
    //根据产品类型名称查询产品信息
    @RequestMapping("/QueryProductTypeAll")
    @ResponseBody
    public JsonBean QueryProductTypeAll(String ProductTypeName){
        try {
            List<Product> products = productService.TypeQueryProductAll(ProductTypeName);
            return ControllerUtil.setSuccess(products);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
    }
}
