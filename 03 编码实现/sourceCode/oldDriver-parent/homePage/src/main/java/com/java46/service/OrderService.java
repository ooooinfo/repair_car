package com.java46.service;

import com.java46.bean.Order;
import com.java46.exception.MyException;
import com.java46.mapper.OrderMapper;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OrderService {
    @Autowired
    private OrderMapper orderMapper;
//    添加预约订单
    public void insertOrder(Order order) throws MyException {
        int insert;
        try {
            insert = orderMapper.insert(order);
        }catch (Exception e){
            throw new MyException("数据库异常");
        }
        ServiceUtil.checkNull(insert,"没有插入成功");
    }
}
