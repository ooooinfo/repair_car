package com.java46.service;

import com.java46.bean.Bespeak;
import com.java46.exception.MyException;
import com.java46.mapper.BespeakMapper;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BespeakService {
    @Autowired
    private BespeakMapper bespeakMapper;
    //添加预约信息
    public void insertBespeak(Bespeak bespeak) throws MyException {
        Integer insert;
        try {
            insert = bespeakMapper.insert(bespeak);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException(e.getMessage());
        }
        ServiceUtil.checkNull(insert,"添加预约信息失败");
    }
}
