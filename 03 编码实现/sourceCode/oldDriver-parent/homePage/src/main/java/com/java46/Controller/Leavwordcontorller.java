package com.java46.Controller;

import com.java46.bean.JsonBean;
import com.java46.bean.LeaveWord;
import com.java46.exception.MyException;
import com.java46.service.LeavwordService;
import com.java46.util.ControllerUtil;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/homepage")
@CrossOrigin(origins = "*" ,maxAge = 3600)
public class Leavwordcontorller {
    @Autowired
    private LeavwordService leavwordService;
    //查询客户留言
    @RequestMapping("/QueryLeavworAll")
    @ResponseBody
    public JsonBean QueryLeavworAll(){
        try {
            List<LeaveWord> leaveWords = leavwordService.QueryLeaveWordAll();
            return ControllerUtil.setSuccess(leaveWords);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail("查询失败");
        }
    }
    //添加客户留言
    @RequestMapping("/InsertLeaveword")
    @ResponseBody
    public JsonBean InsertLeaveword(LeaveWord leaveWord){
        try {
            leavwordService.InsertLeaveword(leaveWord);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail("添加失败");
        }
        return ControllerUtil.setSuccess("添加成功");
    }
}
