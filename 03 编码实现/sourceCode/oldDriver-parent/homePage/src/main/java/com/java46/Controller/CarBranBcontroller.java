package com.java46.Controller;

import com.java46.bean.CarBrand;
import com.java46.bean.JsonBean;
import com.java46.service.CarbrandService;
import com.java46.service.CarbrandService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/homepage")
@CrossOrigin(origins = "*", maxAge = 3600)
public class CarBranBcontroller {
    @Autowired
    private CarbrandService carbrandService;
    @RequestMapping(value = "/QueryCarAll")
    @ResponseBody
    public JsonBean QueryCarAll(){
        List<CarBrand> carBrands = carbrandService.QueryCar();
        JsonBean jsonBean = ControllerUtil.setSuccess(carBrands);
        return jsonBean;
    }
}
