package com.java46.Controller;

import com.aliyuncs.exceptions.ClientException;
import com.java46.RandomNote.RandomNote;
import com.java46.bean.JsonBean;
import com.java46.service.NoteService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/homepage")
@CrossOrigin(origins = "*" , maxAge = 3600)
public class Notecontorller {
    @Autowired
    private NoteService noteService;
    @RequestMapping("/notecontorller")
    @ResponseBody
     public JsonBean notecontorller(String phone){
        RandomNote randomNote = new RandomNote();
        String note;
        try {
            note = noteService.note(phone, randomNote.random());
            if(note == null){
                return ControllerUtil.setFail("系统异常");
            }
        } catch (ClientException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrMsg());
        }
        return ControllerUtil.setSuccess(note);
    }
}
