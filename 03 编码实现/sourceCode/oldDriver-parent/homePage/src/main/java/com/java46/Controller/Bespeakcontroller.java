package com.java46.Controller;

import com.java46.bean.Bespeak;
import com.java46.bean.JsonBean;
import com.java46.exception.MyException;
import com.java46.service.BespeakService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/homepage")
@CrossOrigin(origins = "*",maxAge = 3600)
public class Bespeakcontroller {
    @Autowired
    private BespeakService bespeakService;
    //添加客户预约信息
    @RequestMapping("/insertBespeak")
    @ResponseBody
    public JsonBean insertBespeak(Bespeak bespeak){
        try {
            bespeakService.insertBespeak(bespeak);
        } catch (MyException e) {
            e.printStackTrace();
            ControllerUtil.setFail("添加失败");
        }
        return ControllerUtil.setSuccess("添加成功");
    }
}
