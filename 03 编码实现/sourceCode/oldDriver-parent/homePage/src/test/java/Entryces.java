import com.java46.bean.Admin;
import com.java46.mapper.AdminMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public class Entryces {
    public static void main(String[] args) throws IOException {
        //读取mibatis-config.xml文件
        InputStream resourceAsStream = Resources.getResourceAsStream("mybatis-config.xml");
        //创建sqlsessinfactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        //拿到会话对象
        SqlSession sqlSession = sqlSessionFactory.openSession();
        AdminMapper mapper = sqlSession.getMapper(AdminMapper.class);

        List<Admin> admins = mapper.selectAll();
        for (Admin admin:admins) {
            System.out.println(admin.getAdminAccount()+"----"+admin.getAdminName());
        }
    }
}
