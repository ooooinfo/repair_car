package com.java46.service;

import com.java46.bean.Fault;
import com.java46.bean.Order;
import com.java46.exception.MyException;
import com.java46.mapper.FaultMapper;
import com.java46.mapper.OrderMapper;
import com.java46.util.ServiceUtil;
import com.java46.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ReceptionOrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private FaultMapper faultMapper;

    /**
     * 查询所有订单信息
     * @return
     * @throws MyException
     */
    public List<Order> getAllOrder(String stutasCode) throws MyException {
        List<Order> orderList=null;
        try{
            orderList = orderMapper.queryOrderByStatusCode(Util.codeToHash(stutasCode));
        }catch (Exception e){
            e.getStackTrace();
            throw new MyException("数据库出错了");
        }

        ServiceUtil.checkNull(orderList,"没有查到数据");

        return orderList;
    }

    /**
     * 根据ID删除订单
     * @param id
     * @return
     * @throws MyException
     */
    public boolean deleteOrder(int id) throws MyException {
        if(id==0){
            throw new MyException("该订单不存在！");
        }
        int num = orderMapper.deleteByPrimaryKey(id);
        if(num>0){
            return  true;
        }
        return false;

    }
    /**
     * 查询所有订单信息
     * @return
     * @throws MyException
     */
    public List<Order> getAllMaintenancePlan(String stutasCode) throws MyException {
        List<Order> orderList=null;
        try{
            orderList = orderMapper.queryOrderByStatusCode(Util.codeToHash(stutasCode));
        }catch (Exception e){
            e.getStackTrace();
            throw new MyException("数据库出错了");
        }

        ServiceUtil.checkNull(orderList,"没有查到数据");

        return orderList;
    }

    /**
     * 增加维修计划
     * @param fault
     * @return
     * @throws MyException
     */
    public boolean addMaintenancePlan(Fault fault) throws MyException {
        Order order=null;
        if (fault==null){
            throw  new MyException("不能为空");
        }
        int num =0;
        int nummer=0;
        try {
            num=faultMapper.insert(fault);
            if(num>0){
                order=Util.packageOrder(fault.getOrderId(),"110");
                nummer= orderMapper.updateStatusInOrderid(order);
                return  true;
            }
        }catch (MyException e){
            throw new MyException("添加失败");
        }

        return false;
    }

    /**
     * 修改订单状态
     * @param id
     * @return
     * @throws MyException
     */
    public boolean ConfirmationOfRepairUpdateStatus(int id) throws MyException {
        Order order =null;
        int num=0;
        if (id==0){
            throw  new MyException("没有该订单");
        }
        try {
            order=Util.packageOrder(id,"131");
            num= orderMapper.updateStatusInOrderid(order);
            if(num>0){


                return  true;
            }
        }catch (MyException e){
            throw new MyException("添加失败");
        }
        return  false;

    }
    /**
     * 修改订单状态
     * @param id
     * @return
     * @throws MyException
     */
    public boolean paymentStatus(int id) throws MyException {
        Order order =null;
        int num=0;
        if (id==0){
            throw  new MyException("没有该订单");
        }
        try {
            order=Util.packageOrder(id,"232");
            num= orderMapper.updateStatusInOrderid(order);
            if(num>0){


                return  true;
            }
        }catch (MyException e){
            throw new MyException("添加失败");
        }
        return  false;

    }

}
