package com.java46.service;

import com.java46.bean.Fault;
import com.java46.bean.Order;
import com.java46.bean.Personnel;
import com.java46.exception.MyException;
import com.java46.mapper.FaultMapper;
import com.java46.mapper.OrderMapper;
import com.java46.mapper.PersonnelMapper;
import com.java46.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class RepairService {
    @Autowired
    public OrderMapper orderMapper; //调用订单维修的计划接口
    @Autowired
    public FaultMapper faultMapper; //调用故障信息计划的接口
    @Autowired
    public PersonnelMapper personnelMapper;//调用员工信息计划表

    /*
     * 根据订单状态Id
     * 查询订单功能维修页用户所有订单信息
     * 传入维修页的带检查订单
     * */
    public List<Order> orderMaintenanceByStatuId(String statusCode) throws MyException {
        List<Order> orders = null;
        try {
            //通过状态码查询订单直接，需要把code用Util.codeToHash转为hash表再查询
            orders=orderMapper.queryOrderByStatusCode(Util.codeToHash(statusCode));
        }catch (Exception e) {
            e.printStackTrace();
            throw new MyException("没有查询到相关数据");
        }

        return orders;
    }
    /*
     * 根据订单id
     * 连表查询故障信息表
     * */
    public List<Order> selectFaultByid(Integer orderId) throws MyException {
        List<Order> orders=null;
        try {
            orders = orderMapper.selectOrderFault(orderId);
        }catch (Exception e) {
            e.printStackTrace();
            throw new MyException("没有查询到相关数据");
        }
        return orders;

    }

    /*
     * 根据订单状态Id
     * 查询订单功能维修页用户所有订单信息
     * 传入维修页的以确定订单带维修的订单
     * */
    public List<Order> orderMaintenanceByStatuPage(String statusCode) throws MyException {
        List<Order> orders = null;
        try {
            //通过状态码查询订单直接，需要把code用Util.codeToHash转为hash表再查询
            orders=orderMapper.queryOrderByStatusCode(Util.codeToHash(statusCode));
        }catch (Exception e) {
            e.printStackTrace();
            throw new MyException("没有查询到相关数据");
        }

        return orders;

    }

    /*
     * 根据订单id
     * 连表查询故障信息表
     * */
    public  List<Personnel> selectPersonnelByid(Integer opersonneId) throws MyException {
        List<Personnel> personnels=null;
        try {
            personnels = personnelMapper.selectByPrimaryListKey(opersonneId);
        }catch (Exception e) {
            e.printStackTrace();
            throw new MyException("没有查询到相关数据");
        }
        return personnels;

    }

    /*********************************
     *
     * 将某订单转换为某个状态
     *
     *
     * 需要传入
     * 订单id
     *
     * 状态码
     * 如120,132
     *
     * @param
     * @return
     * @throws MyException
     * ********************************
     */

    public boolean queryOrderByStatusCode(Integer orderId,String statusCode) throws MyException {
        Order order = Util.packageOrder(orderId, statusCode);
        int check;
        try{
            check = orderMapper.updateStatusInOrderid(order);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("oh! 数据库出现了错误");
        }
        if (check==0){
            Util.throwMyexception("修改失败");
        }
        return  true;
    }

    /*
     * 根据维修id删除订单表
     * 传入id
     * 返回Boolean
     * */
    public boolean deleteOrder(int id) throws MyException {
        int order;
        try {
            order = orderMapper.deleteByPrimaryKey(id);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("oh! 数据库出现了错误");
        }

        return false;

    }
    /**
     * 修改维修计划
     * @param fault
     * @return
     * @throws MyException
     */
    public boolean updateMaintenancePlan(Fault fault) throws MyException {
        Order order=null;
        if (fault==null){
            throw  new MyException("不能为空");
        }
        int num =0;
        int nummer=0;
        num=faultMapper.updateByPrimaryKey(fault);
        if(num>0){
            order=Util.packageOrder(fault.getOrderId(),"120");
            nummer= orderMapper.updateStatusInOrderid(order);
            return  true;
        }
        return false;
    }

    /**
     * 修改维修计划页
     * @param
     * @return
     */
    public boolean updateMaintenancePlanstatu(Fault fault) throws MyException {
        Order order = Util.packageOrder(fault.getOrderId(), "132");
        int check;
        try{
            check = orderMapper.updateStatusInOrderid(order);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("oh! 数据库出现了错误");
        }
        if (check==0){
            Util.throwMyexception("修改失败");
        }
        return  true;
    }


}
