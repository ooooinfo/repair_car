package com.java46.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java46.bean.Bespeak;
import com.java46.bean.Fault;
import com.java46.bean.JsonBean;
import com.java46.bean.Order;
import com.java46.exception.MyException;
import com.java46.service.ReceptionOrderService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/function")
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class ReceptionOrderController {
    @Autowired
    private ReceptionOrderService receptionOrderService;

    /**
     * 查询所有未处理的订单信息
     *
     * @return
     */
    @RequestMapping("/selectAllOrder")
    @ResponseBody
    public JsonBean selectAllOrder() {
        List<Order> orderList = null;
        try {
            orderList = receptionOrderService.getAllOrder("100");
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess(orderList);
    }

    /**
     * 添加维修计划
     * @param json
     * @return
     */
    @RequestMapping("/addMaintenancePlan")
    @ResponseBody
    public boolean addMaintenancePlan(String json) {

        boolean bool = false;
//        String json = "[{\"orderId\":1,\"faultInfo\":\"脑袋坏了\"},{\"orderId\":1,\"faultInfo\":\"我也是诶\"}]";
        JSONArray jsonArray = JSON.parseArray(json);
        ArrayList<Fault> list = new ArrayList<Fault>();
        for (int i = 0; i < jsonArray.size(); i++) {
            Fault fault = new Fault();
            fault.setOrderId(Integer.valueOf(jsonArray.getJSONObject(i).getString("orderId")));
            fault.setFaultInfo(jsonArray.getJSONObject(i).getString("faultInfo"));
            list.add(fault);
        }
        for (Fault fault : list) {
            System.out.println(fault.getOrderId() + ":" + fault.getFaultInfo());
            try {
                bool = receptionOrderService.addMaintenancePlan(fault);

            } catch (MyException e) {
                e.printStackTrace();
                return false;
            }

        }


        return bool;
    }

    /**
     * 根据id删除订单信息
     *
     * @param id
     * @return
     */
    @RequestMapping("/delOrder")
    @ResponseBody
    public boolean delOrder(int id) {
        boolean bool = false;

        try {
            bool = receptionOrderService.deleteOrder(id);
        } catch (MyException e) {
            e.printStackTrace();
            return false;
        }
        return bool;
    }

    /**
     * 查询所有维修计划的订单信息
     *
     * @return
     */
    @RequestMapping("/selectAllMaintenancePlan")
    @ResponseBody
    public JsonBean selectAllMaintenancePlan() {
        List<Order> orderList = null;
        try {
            orderList = receptionOrderService.getAllMaintenancePlan("120");
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess(orderList);
    }

    /**
     * 确认修理
     * @param id
     * @return
     */
    @RequestMapping("/ConfirmationOfRepair")
    @ResponseBody
    public boolean ConfirmationOfRepair(int id) {
        boolean bool = false;

        try {
            bool = receptionOrderService.ConfirmationOfRepairUpdateStatus(id);
        } catch (MyException e) {
            e.printStackTrace();
            return false;
        }
        return bool;
    }

    /**
     * 付款
     * @param id
     * @return
     */
    @RequestMapping("/payment")
    @ResponseBody
    public boolean payment(int id) {
        boolean bool = false;

        try {
            bool = receptionOrderService.paymentStatus(id);
        } catch (MyException e) {
            e.printStackTrace();
            return false;
        }
        return bool;
    }

}
