package com.java46.controller;


import com.java46.bean.JsonBean;
import com.java46.bean.Order;
import com.java46.exception.MyException;
import com.java46.service.MaintenanceScheduleService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/function")
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class MaintenanceScheduleController {
    @Autowired
    private MaintenanceScheduleService maintenanceScheduleService;
    /**
     * 查询所有未处理的订单信息
     * @return
     */
    @RequestMapping("/selectAllMaintenanceSchedule")
    @ResponseBody
    public JsonBean selectAllMaintenanceSchedule() {
        List<Order> orderList =null;
        try {
            orderList = maintenanceScheduleService.getAllMaintenanceSchedule("131");
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess(orderList);
    }
}
