package com.java46.service;

import com.java46.bean.Order;
import com.java46.exception.MyException;
import com.java46.mapper.OrderMapper;
import com.java46.util.ServiceUtil;
import com.java46.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class MaintenanceScheduleService {
    @Autowired
    private OrderMapper orderMapper;

    /**
     * 查询所有订单信息
     * @return
     * @throws MyException
     */
    public List<Order> getAllMaintenanceSchedule(String stutasCode) throws MyException {
        List<Order> orderList=null;
        try{
            orderList = orderMapper.queryOrderByStatusCode(Util.codeToHash(stutasCode));
            orderList.addAll(orderMapper.queryOrderByStatusCode(Util.codeToHash("132")));
            orderList.addAll(orderMapper.queryOrderByStatusCode(Util.codeToHash("232")));
        }catch (Exception e){
            e.getStackTrace();
            throw new MyException("数据库出错了");
        }

        ServiceUtil.checkNull(orderList,"没有查到数据");

        return orderList;
    }
}
