package com.java46.service;

import com.java46.bean.Personnel;
import com.java46.exception.MyException;
import com.java46.mapper.PersonnelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class LoginService {
    @Autowired
    public PersonnelMapper personnelMapper;

    public Personnel PersonnelLogin(String username , String pwd) throws MyException {
        Personnel query = null;
        Personnel personnel = new Personnel();
        personnel.setPersonnelAccount(username);
        personnel.setPersonnelPwd(pwd);

        try {
            query = personnelMapper.PersonnelLogin(personnel);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException("没有查询到相关数据");
        }
        return query;

    }
}
