package com.java46.service;

import com.java46.bean.Bespeak;
import com.java46.bean.Order;
import com.java46.exception.MyException;
import com.java46.mapper.BespeakMapper;
import com.java46.mapper.OrderMapper;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class ReceptionBespeakService {
    /**
     * 从spring里拿到mapper接口
     */
    @Autowired
    private BespeakMapper bespeakMapper;
    @Autowired
    private OrderMapper orderMapper;

    /**
     * 查询所有预约
     * @return
     * @throws MyException
     */
    public List<Bespeak> getAllBespeak() throws MyException {
        List<Bespeak> bespeaks=null;
        bespeaks = bespeakMapper.selectAll();
        ServiceUtil.checkNull(bespeaks,"没有查到数据");

        return bespeaks;
    }

    /**
     *查询出预约信息并添加进订单
     * @param id
     */
    public boolean QueryTheAppointmentInformationAndAddTheOrder(int id) throws MyException {
        if(id==0){
           throw new MyException("该预约不存在！");
        }
        Bespeak bespeak =null;
        bespeak=bespeakMapper.selectByPrimaryKey(id);
        ServiceUtil.checkNull(bespeak,"该预约不存在");

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式

        Order order =new Order();
        order.setOrderId(0);
        order.setCarPlate(bespeak.getCarPlate());
        order.setCustomerName(bespeak.getCustomerName());
        order.setCustomerPhone(bespeak.getCustomerPhone());
        order.setOrderTime(df.format(new Date()));
        order.setOrderStatus(1);
        order.setListStatus(0);
        order.setRepairStatus(0);

        int num =0;
        num=orderMapper.insert(order);
        if(num>0){
            return  true;
        }
        return false;
    }

    /**
     * 删除预约
     * @param id
     * @return
     * @throws MyException
     */
    public boolean deleteBespeak(int id) throws MyException {
        if(id==0){
            throw new MyException("该预约不存在！");
        }
        int num = bespeakMapper.deleteByPrimaryKey(id);
        if(num>0){
            return  true;
        }
        return false;

    }

}
