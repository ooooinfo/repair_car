package com.java46.controller;

import com.alibaba.fastjson.JSON;
import com.java46.bean.Bespeak;
import com.java46.bean.JsonBean;
import com.java46.exception.MyException;
import com.java46.service.ReceptionBespeakService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/function")
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class ReceptionBespeakController {
    @Autowired
    private ReceptionBespeakService receptionBespeakService;
    /**
     * 查询所有预约信息
     * @return
     */
    @RequestMapping("/selectAllBespeak")
    @ResponseBody
    public JsonBean selectAllBespeak() {
        List<Bespeak> bespeaks =null;
        try {
            bespeaks = receptionBespeakService.getAllBespeak();
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess(bespeaks);
    }
    @RequestMapping("/queryBespeak")
    @ResponseBody
    public  boolean QueryingBespeakInformationAccordingToID(int id){
        boolean bool = false;

        try {
            bool=receptionBespeakService.QueryTheAppointmentInformationAndAddTheOrder(id);
        } catch (MyException e) {
            e.printStackTrace();
            return  false;
        }
        return bool;
    }
    @RequestMapping("/delBespeak")
    @ResponseBody
    public  boolean delBespeak(int id){
        boolean bool = false;

        try {
            bool=receptionBespeakService.deleteBespeak(id);
        } catch (MyException e) {
            e.printStackTrace();
            return  false;
        }
        return bool;
    }
}
