package com.java46.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.java46.bean.Fault;
import com.java46.bean.JsonBean;
import com.java46.bean.Order;
import com.java46.bean.Personnel;
import com.java46.exception.MyException;
import com.java46.service.RepairService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/function")
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class RepairController {
    @Autowired
    public RepairService repairService;

    /*
     * 根据订单状态Id
     * 查询订单功能维修页用户所有订单信息
     * 传入维修页的带检查订单
     * */
    @RequestMapping("/selectOrderMaintenancePlanByStatu")
    @ResponseBody
    public JsonBean selectOrderMaintenancePlanByid(String statusCode){
        List<Order> order = null;
        String status ="110";
        try {
           order = repairService.orderMaintenanceByStatuId(status);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }

        //封装为Ajax
        return ControllerUtil.setSuccess(order);
    }
    /*
    * 根据订单id
    * 连表查询故障信息表
    * */
    @RequestMapping("/selectFaultByid")
    @ResponseBody
    public JsonBean selectFaultByid(Integer orderId){
        List<Order> orders =null;
        try {
            orders = repairService.selectFaultByid(orderId);
        }catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        //封装为Ajax
        return ControllerUtil.setSuccess(orders);
    }

    /*
     * 根据订单状态Id
     * 查询订单功能维修页用户所有订单信息
     * 传入维修页的以确定订单带维修的订单
     * */
    @RequestMapping("/selectOrderMaintenancePlanByStatuPage")
    @ResponseBody
    public JsonBean selectOrderMaintenancePlanByStatuPage(String statusCode){
        List<Order> order = null;
        String status ="131";
        try {
            order = repairService.orderMaintenanceByStatuPage(status);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }

        //封装为Ajax
        return ControllerUtil.setSuccess(order);

    }

    /*
     * 根据员工id
     * 查询登录员工信息
     * */
    @RequestMapping("/selectPersonnelByid")
    @ResponseBody
    public JsonBean selectPersonnelByid(Integer PersonnelId){
        List<Personnel>  personnel =null;

        int PersonnelIds = 1;
        try {
             personnel = repairService.selectPersonnelByid(PersonnelIds);
        }catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        //封装为Ajax

        return ControllerUtil.setSuccess(personnel);
    }
    /**********************************************
     *
     * 改变订单状态
     * 状态120
     *
     * 需要传入【
     *  orderId
     *  statusCode
     * 】
     */
    @RequestMapping("/changeStatusowo")
    @ResponseBody
    public JsonBean changeStatus(Integer orderId, String statusCode){
        int orderIds = 1;
        String statuCodeowo = "120";
        try {
            repairService.queryOrderByStatusCode(orderIds,statuCodeowo);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess("修改成功");
    }
    /**********************************************
     *
     * 改变订单状态
     * 状态132
     *
     * 需要传入【
     *  orderId
     *  statusCode
     * 】
     */
    @RequestMapping("/changeStatusotw")
    @ResponseBody
    public JsonBean changeStatusotw(Integer orderId, String statusCode){
        int orderIds = 1;
        String statuCodeowo = "132";
        try {
            repairService.queryOrderByStatusCode(orderIds,statuCodeowo);
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess("修改成功");
    }
    /*
    * 根据维修id删除订单表
    * 传入id
    * 返回Boolean
    * */
    @RequestMapping("/delateById")
    @ResponseBody
    public  boolean delOrder(int id){
        boolean bool = false;
        try {
            bool=repairService.deleteOrder(id);
        } catch (MyException e) {
            e.printStackTrace();
            return  false;
        }
        return bool;
    }
    /**
     * 修改维修计划
     * @param json
     * @return
     */
    @RequestMapping("/updateMaintenancePlans")
    @ResponseBody
    public boolean addMaintenancePlan(String json) {

        boolean bool = false;
        //解析传递前台的Json字符串
        JSONArray jsonArray = JSON.parseArray(json);
        ArrayList<Fault> list = new ArrayList<Fault>();
        for (int i = 0; i < jsonArray.size(); i++) {
            Fault fault = new Fault();
            fault.setOrderId(Integer.valueOf(jsonArray.getJSONObject(i).getString("orderId")));
            fault.setFaultInfo(jsonArray.getJSONObject(i).getString("faultDetail"));
            list.add(fault);
        }
        for (Fault fault : list) {
            try {
                bool = repairService.updateMaintenancePlan(fault);

            } catch (MyException e) {
                e.printStackTrace();
                return false;
            }

        }


        return bool;
    }
    /**
     * 修改维修计划页
     * @param json
     * @return
     */
    @RequestMapping("/updateMaintenancePlanstatu")
    @ResponseBody
    public boolean updateMaintenancePlanstatu(String json) {

        boolean bool = false;
        //解析传递前台的Json字符串
        JSONArray jsonArray = JSON.parseArray(json);
        ArrayList<Fault> list = new ArrayList<Fault>();
        for (int i = 0; i < jsonArray.size(); i++) {
            Fault fault = new Fault();
            fault.setOrderId(Integer.valueOf(jsonArray.getJSONObject(i).getString("orderId")));
            fault.setFaultInfo(jsonArray.getJSONObject(i).getString("faultDetail"));
            list.add(fault);
        }
        for (Fault fault : list) {
            try {
                bool = repairService.updateMaintenancePlanstatu(fault);

            } catch (MyException e) {
                e.printStackTrace();
                return false;
            }

        }


        return bool;
    }







}
