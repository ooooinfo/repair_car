package com.java46.controller;

import com.java46.bean.JsonBean;
import com.java46.bean.Personnel;
import com.java46.exception.MyException;
import com.java46.service.LoginService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
/*
* 登录功能页面
* */
@RestController
@RequestMapping("/function")
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class RepairLoginController {
    @Autowired
    public LoginService loginService;

    @RequestMapping("/repairlogin")
    @ResponseBody
    public JsonBean PersonnelLogin(String username, String pwd){
        Personnel login=null;
        try{
            login= loginService.PersonnelLogin(username, pwd);
        }catch (MyException e){
            e.printStackTrace();

            return  ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess(login);
    }


}
