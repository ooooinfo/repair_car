package com.java46.controller;

import com.java46.bean.JsonBean;
import com.java46.bean.Personnel;
import com.java46.exception.MyException;
import com.java46.service.PersonnelService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class PersonnelController {

    @Autowired
    private PersonnelService personnelService;

    /**
     * 查询所有员工信息
     *
     * 请求地址 http://localhost:8080/admin/queryAllPersonnel.do
     *
     * @return
     */
    @RequestMapping("queryAllPersonnel")
    @ResponseBody
    public JsonBean queryAllPer(){

        List<Personnel> allPersonnel = null;
        try {
            //执行service的查询方法
            allPersonnel = personnelService.queryAllPersonnel();
        } catch (MyException e) {
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(allPersonnel);
    }


    /**
     * 根据员工编号查询员工信息
     *
     * 请求地址 http://localhost:8080/admin/queryByPersonnelId.do
     *
     * 需要传入【personnelId】
     *
     * @param personnelId 员工编号
     * @return
     */
    @RequestMapping("queryByPersonnelId")
    @ResponseBody
    public JsonBean queryByPersonnelId(Integer personnelId){
        Personnel personnel;
        try{
            //执行service 的根据编号查询方法
            personnel = personnelService.queryByPersonnelId(personnelId);
        }catch (Exception e){
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getMessage());
        }
        // 成功后自动封装正确数据，return
        return  ControllerUtil.setSuccess(personnel);
    }

    /**
     * 添加员工
     *
     * 请求地址 http://localhost:8080/admin/addPersonnel.do
     *
     * 需要传入【
     *  personnelAccount
     *  personnelPwd
     *  personnelName
     *  personnelImg
     *  personnelType
     *  】
     *
     * @param personnel
     * @return
     */
    @RequestMapping("/addPersonnel")
    @ResponseBody
    public JsonBean addPersonnel(Personnel personnel){
        Integer result = 0;
        try {
            //执行service的添加方法
            result = personnelService.addPersonnel(personnel);
        } catch (MyException e) {
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(result);
    }

    /**
     * 根据员工编号删除员工信息
     *
     * 请求地址 http://localhost:8080/admin/deletePersonnel.do
     *
     * 需要传入【personnelId】
     *
     * @param personnelId 员工编号
     * @return
     */
    @RequestMapping("deletePersonnel")
    @ResponseBody
    public JsonBean deletePersonnel(Integer personnelId){
        Integer result = 0;
        try {
            //执行service的删除方法
            result = personnelService.deletePersonnel(personnelId);
        } catch (MyException e) {
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(result);
    }

    /**
     * 修改员工信息
     *
     * 请求地址 http://localhost:8080/admin/updatePersonnel.do
     *
     * 需要传入【
     *   personnelAccount
     *   personnelPwd
     *   personnelName
     *   personnelImg
     *   personnelType
     * 】
     * @param personnel 员工对象
     * @return
     */
    @RequestMapping("updatePersonnel")
    @ResponseBody
    public JsonBean updatePersonnel(Personnel personnel){
        Integer result = 0;
        try {
            //执行service的修改方法
            result = personnelService.updatePersonnel(personnel);
        }catch (Exception e){
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getMessage());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(result);
    }

}
