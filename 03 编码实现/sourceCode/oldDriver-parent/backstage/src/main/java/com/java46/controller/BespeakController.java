package com.java46.controller;

import com.java46.bean.Bespeak;
import com.java46.bean.JsonBean;
import com.java46.exception.MyException;
import com.java46.service.BespeakService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class BespeakController {

    @Autowired
    private BespeakService bespeakService;

    /***************************************
     * 查询所有预约信息
     *
     * 请求地址 http://localhost:8080/admin/queryAllBespeak.do
     *
     * @return
     ***************************************/
    @RequestMapping("queryAllBespeak")
    @ResponseBody
    public JsonBean queryAllBespeak(){

        List<Bespeak> bespeaks;
        try {
            //执行service的查询方法
            bespeaks = bespeakService.queryAllBespeak();
        } catch (MyException e) {
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(bespeaks);
    }


    /*********************************************
     * 根据预约编号查询预约信息
     *
     * 请求地址 http://localhost:8080/admin/queryByBespeakId.do
     *
     * 需要传入【bespeakId】
     *
     * @param bespeakId 预约编号
     * @return
     *********************************************/
    @RequestMapping("queryByBespeakId")
    @ResponseBody
    public JsonBean queryByBespeakId(Integer bespeakId){
        Bespeak bespeak;
        try{
            //执行service 的根据编号查询方法
            bespeak = bespeakService.queryByBespeakId(bespeakId);
        }catch (Exception e){
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getMessage());
        }
        // 成功后自动封装正确数据，return
        return  ControllerUtil.setSuccess(bespeak);
    }


    /******************************************
     * 添加预约
     *
     * 请求地址 http://localhost:8080/admin/addBespeak.do
     *
     * 需要传入【
     *  bespeakTime
     *  carPlate
     *  customerName
     *  customerPhone
     *  state
     * 】
     *
     * @param bespeak
     * @return
     ******************************************/
    @RequestMapping("/addBespeak")
    @ResponseBody
    public JsonBean addBespeak(Bespeak bespeak){
        Integer result = 0;
        try {
            //执行service的添加方法
            result = bespeakService.addBespeak(bespeak);
        } catch (MyException e) {
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(result);
    }


    /*******************************************
     * 根据预约编号删除预约信息
     *
     * 请求地址 http://localhost:8080/admin/deleteBespeak.do
     *
     * 需要传入【bespeakId】
     *
     * @param bespeakId  预约编号
     * @return
     **********************************************/
    @RequestMapping("deleteBespeak")
    @ResponseBody
    public JsonBean deleteBespeak(Integer bespeakId){
        Integer result = 0;
        try {
            //执行service的删除方法
            result = bespeakService.deleteBespeak(bespeakId);
        } catch (MyException e) {
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(result);
    }


    /*****************************************
     * 修改预约信息
     *
     * 请求地址 http://localhost:8080/admin/updateBespeak.do
     *
     * 需要传入【
     *   bespeakTime
     *   carPlate
     *   customerName
     *   customerPhone
     *   state
     * 】
     *
     * @param personnel 预约对象
     * @return
     *******************************************/
    @RequestMapping("updateBespeak")
    @ResponseBody
    public JsonBean updateBespeak(Bespeak personnel){
        Integer result = 0;
        try {
            //执行service的修改方法
            result = bespeakService.updateBespeak(personnel);
        }catch (Exception e){
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getMessage());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(result);
    }

}

