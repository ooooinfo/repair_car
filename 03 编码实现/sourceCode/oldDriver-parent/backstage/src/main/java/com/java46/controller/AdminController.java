package com.java46.controller;

import com.java46.bean.Admin;
import com.java46.bean.JsonBean;
import com.java46.exception.MyException;
import com.java46.service.AdminService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class AdminController {
    @Autowired
    private AdminService adminService;

    /**
     * 查询所有管理员
     * @return
     */
    @RequestMapping("seleAllAdmin")
    @ResponseBody
    public JsonBean seleAllAdmin(){
        List<Admin> admins = null;
        try{
            admins = adminService.getAllAdmin();
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }
        return ControllerUtil.setSuccess(admins);
    }

    /**
     * 根据管理员编号查询管理员
     * @param adminId
     * @return
     */
    @RequestMapping("/seleAllAdminById")
    @ResponseBody
    public JsonBean seleAllAdminById(Integer adminId){
        Admin admin= null;
        try{
            admin = adminService.selectAdminById(adminId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }
        return ControllerUtil.setSuccess(admin);
    }

    /**
     * 添加管理员
     * @param admin
     * @return
     */
    @RequestMapping("/InsertAdmin")
    @ResponseBody
    public JsonBean InsertAdmin(Admin admin){
        Integer result = 0;
        try{
            result = adminService.InsertAdmin(admin);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }
        return ControllerUtil.setSuccess(result);
    }

    /**
     * 根据管理员编号，删除管理员
     * @param adminId
     * @return
     */
    @RequestMapping("/deleteAdminById")
    @ResponseBody
    public JsonBean deleteAdminById(Integer adminId){
        Integer result = 0;
        try{
            result = adminService.DeleteAdminbyId(adminId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }
        return ControllerUtil.setSuccess(result);
    }

    @RequestMapping("/UpdateAdminById")
    @ResponseBody
    public JsonBean UpdateAdminById(Admin adminId){
        Integer result = 0;
        try{
            result = adminService.UpdateAdminById(adminId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }
        return ControllerUtil.setSuccess(result);
    }

    @RequestMapping("/LoginAdmin")
    @ResponseBody
    public JsonBean LoginAdmin(String adminAccount,String AdminPwd){
        Admin login=null;
        try{
            login= adminService.AdminLogin(adminAccount, AdminPwd);
        }catch (MyException e){
            e.printStackTrace();

            return  ControllerUtil.setFail(e.getErrorMsg());
        }
        return ControllerUtil.setSuccess(login);
    }
}
