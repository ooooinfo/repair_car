package com.java46.controller;

import com.java46.bean.JsonBean;
import com.java46.bean.ProductType;
import com.java46.service.ProductTypeService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@CrossOrigin(origins = "*", maxAge = 3600)
public class ProductTypeController {
   @Autowired
    private ProductTypeService productTypeService;

    /**
     * 查询所有产品类型
     *
     * 请求地址：http://localhost:8080/ProductType/selectAllProType.do
     * @return
     */
    @RequestMapping("/selectAllProType")
    @ResponseBody
    public JsonBean selectAllProType(){
       List<ProductType> allProType = null;
       try{
           allProType = productTypeService.getAllProductType();
       }catch (Exception e){
           e.printStackTrace();
           return ControllerUtil.setFail(e.getMessage());
       }

       return ControllerUtil.setSuccess(allProType);
   }

    /**
     * 根据Id 查询产品类型
     *
     * 请求地址：http://localhost:8080/ProductType/getProductTypeById.do
     *
     * 传入参数[
     *      productTypeId
     * ]
     * @param productTypeId
     * @return
     */
    @RequestMapping("/getProductTypeById")
    @ResponseBody
    public JsonBean getProductTypeById(Integer productTypeId){
        ProductType productType = null;
        try{
            productType = productTypeService.selectProductTypeById(productTypeId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }

        return ControllerUtil.setSuccess(productType);
    }

    /**
     * 添加产品类型
     *
     * 请求地址：http://localhost:8080/ProductType/InsertProductType.do
     *
     * 传入参数[
     *     productTypeId
     *     productTypeName
     * ]
     *
     * @param productType
     * @return
     */
    @RequestMapping("/InsertProductType")
    @ResponseBody
    public JsonBean InsertProductType(ProductType productType){
        Integer result = 0;
        try{
            result = productTypeService.insertProductType(productType);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }

        return ControllerUtil.setSuccess(result);
    }

    /**
     * 删除产品类型
     *
     * 请求地址：http://localhost:8080/ProductType/deleteProductType.do
     *
     *  传入参数[
     *      productTypeId
     * ]
     * @param productTypeId
     * @return
     */
    @RequestMapping("/deleteProductType")
    @ResponseBody
    public JsonBean deleteProductType(Integer productTypeId){
        Integer result = 0;
        try{
            result = productTypeService.deleteProductType(productTypeId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }

        return ControllerUtil.setSuccess(result);
    }

    /**
     * 根据品牌编号，修改品牌信息
     *
     * 请求地址：http://localhost:8080/ProductType/UpdateProductTypeById.do
     *
     * 传入参数[
     *      productTypeId
     * ]
     * @param productTypeId
     * @return
     */
    @RequestMapping("/UpdateProductTypeById")
    @ResponseBody
    public JsonBean UpdateProductTypeById(ProductType productTypeId){
        Integer result = 0;
        try{
            result = productTypeService.UpdateProducttTypeById(productTypeId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }

        return ControllerUtil.setSuccess(result);
    }
}
