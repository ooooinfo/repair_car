package com.java46.controller;

import com.java46.bean.CarBrand;
import com.java46.bean.JsonBean;
import com.java46.service.CarBrandService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/CarBrand")
@CrossOrigin(origins = "*" ,maxAge = 3600)
public class CarBrandController {
    @Autowired
    private CarBrandService carBrandService;

    /**
     *
     * 查询所有汽车品牌信息
     * 请求地址：http://localhost:8080/CarBrand/seleAllCarBrand.do
     * @return
     */
    @RequestMapping("/seleAllCarBrand")
    @ResponseBody
    public JsonBean seleAllCarBrand(){
        List<CarBrand> carBrands = null;
        try{
            carBrands = carBrandService.getAllCarBrand();
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }
        return ControllerUtil.setSuccess(carBrands);
    }

    /**
     * 根据汽车品牌Id 查询汽车品牌
     *
     * 请求地址：http://localhost:8080/CarBrand/seleAllCarBrand.do
     *
     * 传入参数[
     *      carBrandId
     * ]
     * @param carBrandId
     * @return
     */
    @RequestMapping("/selectCarBrandById")
    @ResponseBody
    public JsonBean selectCarBrandById(Integer carBrandId){
        CarBrand carBrand ;
        try{
            carBrand = carBrandService.selectCarBrandById(carBrandId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }
        return ControllerUtil.setSuccess(carBrand);
    }

    /**
     * 添加汽车品牌
     *
     * 请求地址：http://localhost:8080/CarBrand/InsertCarBrand.do
     *
     * 传入参数[
     *      carBrandId
     *      carBrandName
     *      carBrandImg
     * ]
     * @param carBrand
     * @return
     */
    @RequestMapping("/InsertCarBrand")
    @ResponseBody
    public JsonBean InsertCarBrand(CarBrand carBrand){
        Integer result = 0;
        try{
            result = carBrandService.InsertCarBrand(carBrand);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }
        return ControllerUtil.setSuccess(result);
    }

    /**
     * 根据汽车品牌Id 删除品牌信息
     *
     * 请求地址：http://localhost:8080/CarBrand/deleteCarBrandById.do
     *
     * 传入参数[
     *      carBrandId
     * ]
     * @param carBrandId
     * @return
     */
    @RequestMapping("/deleteCarBrandById")
    @ResponseBody
    public JsonBean deleteCarBrandById(Integer carBrandId){
        Integer result = 0;
        try{
            result = carBrandService.deleteCarBrand(carBrandId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }

        return ControllerUtil.setSuccess(result);
    }

    /**
     * 根据汽车品牌Id 修改汽车品牌信息
     *
     * 请求地址：http://localhost:8080/CarBrand/UpdateCarBrandById.do
     *
     * 传入参数[
     *      carBrandId
     * ]
     * @param carBrandId
     * @return
     */
    @RequestMapping("/UpdateCarBrandById")
    @ResponseBody
    public JsonBean UpdateCarBrandById(CarBrand carBrandId){
        Integer result = 0;
        try{
            result = carBrandService.UpdateCarBrand(carBrandId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }

        return ControllerUtil.setSuccess(result);
    }
}