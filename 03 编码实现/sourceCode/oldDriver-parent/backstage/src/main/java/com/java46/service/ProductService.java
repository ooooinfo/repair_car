package com.java46.service;

import com.java46.bean.Product;
import com.java46.exception.MyException;
import com.java46.mapper.ProductMapper;
import com.java46.mapper.ProductTypeMapper;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ProductService {
    /**
     * 从spring得mapper接口
     */
    @Autowired
    private ProductMapper productMapper;

    /**
     * 查询所有产品
     * @return
     * @throws MyException
     */
    public List<Product> getAllProduct() throws MyException {
        List<Product> products;

        products = productMapper.selectAll();
        ServiceUtil.checkListNull(products,"没有查到数据！");

        return products;
    }

    /**
     * 根据产品编号查询产品信息
     * @param productId
     * @return
     * @throws MyException
     */
    public Product selectByProductId(Integer productId) throws MyException {
        ServiceUtil.checkNull(productId,"产品编号不能为空");
        Product product;
                try {
                    product = productMapper.selectByPrimaryKey(productId);
                }catch (Exception e){
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(product,"没有此产品的编号信息");
        return product;
    }

    /**
     * 添加产品
     * @param product
     * @return
     * @throws MyException
     */
    public Integer InsertProduct(Product product) throws MyException {
        ServiceUtil.checkNull(product,"产品不能为空！");
        Integer result;
                try {
                    result = productMapper.insert(product);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(result,"产品添加失败！");

        return result;
    }

    /**
     * 根据产品编号删除产品
     * @param productId
     * @return
     * @throws MyException
     */
    public Integer DeleteByProductId(Integer productId) throws MyException {
        ServiceUtil.checkNull(productId,"编号不能为空！");
        Integer result;
                try {
                    result = productMapper.deleteByPrimaryKey(productId);
                }catch (Exception e) {
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
                ServiceUtil.checkNull(result, "没有此商品编号，不能删除");

        return result;
    }

    /**
     * 根据产品编号修改产品信息
     * @param productId
     * @return
     * @throws MyException
     */
    public Integer UpdateByProductId(Product productId) throws MyException {
        ServiceUtil.checkNull(productId,"编号不能为空！");
        Integer result;
                try {
                    result = productMapper.updateByPrimaryKey(productId);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(result,"没有此商品编号，不能做修改~");

        return result;
    }
}
