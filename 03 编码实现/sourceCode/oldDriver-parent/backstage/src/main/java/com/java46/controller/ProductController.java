package com.java46.controller;

import com.java46.bean.JsonBean;
import com.java46.bean.Product;
import com.java46.exception.MyException;
import com.java46.service.ProductService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class ProductController {
    @Autowired
    private ProductService productService;

    /**
     * 查询所有产品
     * 请求地址：http://localhost:8080/Product/selectAllPro.do
     * @return
     */
    @RequestMapping("/selectAllPro")
    @ResponseBody
    public JsonBean selectAllPro(){
        List<Product> allProduct = null;
        try {
            allProduct= productService.getAllProduct();
        } catch (MyException e) {
            e.printStackTrace();
            return ControllerUtil.setFail(e.getErrorMsg());
        }

        return ControllerUtil.setSuccess(allProduct);
    }

    /**************************
     * 根据产品编号查询产品
     *
     * 请求地址：http://localhost:8080/Product/getProductId.do
     *
     * 传入参数：[
     *     productId
     * ]
     * @return
     **************************/
    @RequestMapping("/getProductId")
    @ResponseBody
    public JsonBean getProductId(Integer productId){
        Product product = null;
        try{
            //执行service 的查询方法
            product = productService.selectByProductId(productId);
        }catch (Exception e){
            e.printStackTrace();
             return ControllerUtil.setFail(e.getMessage());
        }
        return  ControllerUtil.setSuccess(product);
    }

    /*******************************
     * 添加产品
     *
     * 请求地址：http://localhost:8080/Product/InsertProduct.do
     *
     * 传入参数：[
     *    productId
     *    productName
     *    productNum
     *    productPrice
     *    productImg
     *    productTypeId
     *  ]
     * @return
     *******************************/
    @RequestMapping("/InsertProduct")
    @ResponseBody
    public JsonBean InsertProduct(Product product){
        Integer result = 0;
        try{
            result = productService.InsertProduct(product);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }

        return ControllerUtil.setSuccess(result);
    }

    /**************************
     * 根据id删除产品
     *
     * 请求地址：http://localhost:8080/Product/DeleteProductById.do
     *
     * 传入参数：[
     *      productId
     * ]
     * @return
     **************************/
    @RequestMapping("/DeleteProductById")
    @ResponseBody
    public JsonBean DeleteProductById(Integer productId){
        Integer result = 0;
        try{
            result = productService.DeleteByProductId(productId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }

        return ControllerUtil.setSuccess(result);
    }

    /***************************
     * 根据产品编号修改产品
     *
     * 请求地址：http://localhost:8080/Product/UpdateProductById.do
     *
     * 传入参数：[
     *      productId
     * ]
     * @param productId
     * @return
     ***************************/
    @RequestMapping("/UpdateProductById")
    @ResponseBody
    public JsonBean UpdateProductById(Product productId){
        Integer result = 0;
        try{
            result = productService.UpdateByProductId(productId);
        }catch (Exception e){
            e.printStackTrace();
            return ControllerUtil.setFail(e.getMessage());
        }

        return  ControllerUtil.setSuccess(result);
    }

}
