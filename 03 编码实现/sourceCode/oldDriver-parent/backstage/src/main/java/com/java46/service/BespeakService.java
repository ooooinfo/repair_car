package com.java46.service;

import com.java46.bean.Bespeak;
import com.java46.exception.MyException;
import com.java46.mapper.BespeakMapper;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class BespeakService {

    @Autowired
    private BespeakMapper bespeakMapper;

    /**
     * 查询预约信息
     * @return 返回一个集合
     * @throws MyException
     */
    public List<Bespeak> queryAllBespeak() throws MyException {
        List<Bespeak> bespeak;
        bespeak = bespeakMapper.selectAll();
        ServiceUtil.checkListNull(bespeak,"数据库没有预约信息！");

        return bespeak;
    }

    /**
     * 根据预约编号查询预约信息
     * @param bespeakId 预约编号
     * @return 返回对象
     * @throws MyException
     */
    public Bespeak queryByBespeakId(Integer bespeakId) throws MyException {

        ServiceUtil.checkNull(bespeakId,"预约编号不能为空！");
        Bespeak bespeak;
        try {
            bespeak = bespeakMapper.selectByPrimaryKey(bespeakId);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据异常！");
        }
        ServiceUtil.checkNull(bespeak,"根据预约编号查询预约信息失败！");

        return bespeak;
    }

    /**
     * 添加预约
     * @param bespeak 预约对象
     */
    public Integer addBespeak(Bespeak bespeak) throws MyException {

        ServiceUtil.checkNull(bespeak,"预约不能为空！");
        Integer result ;
        try {
            result = bespeakMapper.insert(bespeak);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据异常！");
        }
        ServiceUtil.checkNull(result,"预约信息添加失败！");

        return result;
    }

    /**
     * 根据预约编号删除预约信息
     * @param bespeakId 预约编号
     */
    public Integer deleteBespeak(Integer bespeakId) throws MyException {

        ServiceUtil.checkNull(bespeakId,"预约编号不能为空！");
        Integer result;
        try {
            result =  bespeakMapper.deleteByPrimaryKey(bespeakId);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据异常！");
        }
        ServiceUtil.checkNull(result,"没有此预约编号，删除失败！");

        return result;
    }

    /**
     * 修改预约信息
     * @param bespeak 预约对象
     */
    public Integer updateBespeak(Bespeak bespeak) throws MyException {

        ServiceUtil.checkNull(bespeak,"预约不能为空！");
        Integer result ;
        try {
            result =  bespeakMapper.updateByPrimaryKey(bespeak);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据异常！");
        }
        ServiceUtil.checkNull(result,"预约信息修改失败！");

        return result;
    }
}
