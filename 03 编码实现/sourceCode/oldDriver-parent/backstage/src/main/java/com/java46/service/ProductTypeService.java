package com.java46.service;

import com.java46.bean.ProductType;
import com.java46.exception.MyException;
import com.java46.mapper.ProductTypeMapper;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ProductTypeService {
    @Autowired
    private ProductTypeMapper productTypeMapper;

    /**
     * 查询所有产品类型信息
     * @return
     * @throws MyException
     */
    public List<ProductType> getAllProductType() throws MyException {
        List<ProductType> productTypes = productTypes = productTypeMapper.selectAll();
        ServiceUtil.checkListNull(productTypes,"没有查到此产品类型");
        return productTypes;
    }

    /**
     * 根据产品id，查询产品
     * @param productTypeId
     * @return
     * @throws MyException
     */
    public ProductType selectProductTypeById(Integer productTypeId) throws MyException {
        ServiceUtil.checkNull(productTypeId,"编号不能为空");
        ProductType productType;
                try {
                    productType = productTypeMapper.selectByPrimaryKey(productTypeId);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(productTypeId,"没有此产品的信息");
        return productType;
    }

    /**
     * 添加产品类型
     * @param productType
     * @return
     * @throws MyException
     */
    public int insertProductType(ProductType productType) throws MyException {
        ServiceUtil.checkNull(productType,"产品不能为空");
        Integer result;
                try {
                    result = productTypeMapper.insert(productType);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(result,"添加产品类型失败");
        return result;
    }

    /**
     *根据id 删除产品
     * @param productTypeId
     * @return
     * @throws MyException
     */
    public Integer deleteProductType(Integer productTypeId) throws MyException {
        ServiceUtil.checkNull(productTypeId,"类型编号不能为空");
        Integer result;
                try {
                    result = productTypeMapper.deleteByPrimaryKey(productTypeId);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(result,"删除产品类型失败！");
        return result;
    }

    /**
     * 根据产品id 修改商品类型
     * @param productType
     * @return
     * @throws MyException
     */
    public Integer UpdateProducttTypeById(ProductType productType) throws MyException {
        ServiceUtil.checkNull(productType,"类型编号不能为空");
        Integer result;
                try {
                    result = productTypeMapper.updateByPrimaryKey(productType);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(result,"修改产品类型失败！");
        return result;
    }
}
