package com.java46.service;

import com.java46.bean.Admin;
import com.java46.exception.MyException;
import com.java46.mapper.AdminMapper;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class AdminService {
    /**
     * 从spring得mapper接口
     */
    @Autowired
    private AdminMapper adminMapper;

    /**
     * 查询所有管理员
     * @return
     * @throws MyException
     */
    public List<Admin> getAllAdmin() throws MyException {
        List<Admin> admins;

        admins = adminMapper.selectAll();
        ServiceUtil.checkListNull(admins,"没有查到数据！");

        return admins;
    }

    /**
     * 根据管理员编号查询管理员信息
     * @param adminId
     * @return
     * @throws MyException
     */
    public Admin selectAdminById(Integer adminId) throws MyException {
        ServiceUtil.checkNull(adminId,"管理员编号不能为空");
        Admin admin;
        try {
            admin = adminMapper.selectByPrimaryKey(adminId);
        }catch (Exception e){
            throw new MyException("数据异常");
        }
        ServiceUtil.checkNull(admin,"没有此产品的编号信息");
        return admin;
    }

    /**
     * 添加管理员
     * @param admin
     * @return
     * @throws MyException
     */
    public Integer InsertAdmin(Admin admin) throws MyException {
        ServiceUtil.checkNull(admin,"管理员不能为空！");
        Integer result;
        try {
            result = adminMapper.insert(admin);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据异常");
        }
        ServiceUtil.checkNull(result,"管理员添加失败！");

        return result;
    }

    /**
     * 根据管理员编号删除管理员
     * @param adminId
     * @return
     * @throws MyException
     */
    public Integer DeleteAdminbyId(Integer adminId) throws MyException {
        ServiceUtil.checkNull(adminId,"编号不能为空！");
        Integer result;
        try {
            result = adminMapper.deleteByPrimaryKey(adminId);
        }catch (Exception e) {
            e.printStackTrace();
            throw new MyException("数据异常");
        }
        ServiceUtil.checkNull(result, "没有此管理员编号，不能删除");

        return result;
    }

    /**
     * 根据管理员编号修改管理员信息
     * @param adminId
     * @return
     * @throws MyException
     */
    public Integer UpdateAdminById(Admin adminId) throws MyException {
        ServiceUtil.checkNull(adminId,"编号不能为空！");
        Integer result;
        try {
            result = adminMapper.updateByPrimaryKey(adminId);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException("数据异常");
        }
        ServiceUtil.checkNull(result,"没有此管理员编号，不能做修改~");

        return result;
    }

    public Admin AdminLogin(String adminAccount , String Adminpwd) throws MyException {
        Admin query = null;
        Admin admin = new Admin();
        admin.setAdminAccount(adminAccount);
        admin.setAdminPwd(Adminpwd);

        try {
            query = adminMapper.AdminLogin(admin);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException("没有查询到相关数据");
        }
        return query;

    }
}
