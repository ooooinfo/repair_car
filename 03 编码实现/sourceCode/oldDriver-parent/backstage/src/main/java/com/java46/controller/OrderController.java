package com.java46.controller;

import com.java46.bean.Bespeak;
import com.java46.bean.JsonBean;
import com.java46.bean.Order;
import com.java46.exception.MyException;
import com.java46.service.OrderService;
import com.java46.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
//全局跨域
@CrossOrigin(origins = "*", maxAge = 3600)
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**************************************
     * 查询所有订单信息
     *
     * 请求地址 http://localhost:8080/admin/queryAllOrder.do
     *
     * @return
     **************************************/
    @RequestMapping("queryAllOrder")
    @ResponseBody
    public JsonBean queryAllOrder(){
        List<Order> orders;
        try {
            //执行service的查询方法
            orders = orderService.queryAllOrder();
        } catch (MyException e) {
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(orders);
    }


    /***************************************
     * 根据订单编号查询所有订单信息
     *
     * 请求地址 http://localhost:8080/admin/queryByOrderId.do
     *
     * 需要传入【orderId】
     *
     * @param orderId 订单编号
     * @return
     ******************************************/
    @RequestMapping("queryByOrderId")
    @ResponseBody
    public JsonBean queryByOrderId(Integer orderId){
        Order order = null;
        try {
            //执行service的根据编号查询方法
            order = orderService.queryByOrderId(orderId);
        } catch (MyException e) {
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getMessage());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(order);
    }


    /*********************************************
     * 添加订单
     *
     * 请求地址 http://localhost:8080/admin/addOrder.do
     *
     * 需要传入【
     *  orderTime
     *  carPlate
     *  customerName
     *  customerPhone
     *  orderStatus
     *  listStatus
     *  repairStatus
     * 】
     *
     * @param order
     * @return
     **********************************************/
    @RequestMapping("addOrder")
    @ResponseBody
    public JsonBean addOrder(Order order){
        Integer result = 0;
        try {
            //执行service的添加方法
            result = orderService.addOrder(order);
        } catch (MyException e) {
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(result);
    }


    /***********************************************
     * 根据订单编号删除订单信息
     *
     * 请求地址 http://localhost:8080/admin/deleteOrder.do
     *
     * 需要传入【orderId】
     *
     * @param orderId  订单编号
     * @return
     ***********************************************/
    @RequestMapping("deleteOrder")
    @ResponseBody
    public JsonBean deleteOrder(Integer orderId){
        Integer result = 0;
        try {
            //执行service的删除方法
            result = orderService.deleteOrder(orderId);
        } catch (MyException e) {
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getErrorMsg());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(result);
    }


    /******************************************
     * 修改订单信息
     *
     * 请求地址 http://localhost:8080/admin/updateOrder.do
     *
     * 需要传入【
     *  orderTime
     *  carPlate
     *  customerName
     *  customerPhone
     *  orderStatus
     *  listStatus
     *  repairStatus
     * 】
     *
     * @param order 订单对象
     * @return
     ******************************************/
    @RequestMapping("updateOrder")
    @ResponseBody
    public JsonBean updateOrder(Order order){
        Integer result = 0;
        try {
            //执行service的修改方法
            result = orderService.updateOrder(order);
        }catch (Exception e){
            e.printStackTrace();
            // 失败时输入异常，并封装错误信息，return
            return ControllerUtil.setFail(e.getMessage());
        }
        // 成功后自动封装正确数据，return
        return ControllerUtil.setSuccess(result);
    }
}
