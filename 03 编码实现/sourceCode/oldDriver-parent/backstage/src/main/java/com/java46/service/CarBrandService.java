package com.java46.service;

import com.java46.bean.CarBrand;
import com.java46.exception.MyException;
import com.java46.mapper.CarBrandMapper;
import com.java46.util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class CarBrandService {
    @Autowired
    private CarBrandMapper carBrandMapper;

    /**
     * 查询所有汽车品牌
     * @return
     * @throws MyException
     */
    public List<CarBrand> getAllCarBrand() throws MyException {
        List<CarBrand> carBrands;
        carBrands = carBrandMapper.selectAll();
        ServiceUtil.checkListNull(carBrands,"没有汽车品牌信息！");

        return carBrands;
    }

    /**
     * 根据汽车品牌id 查询汽车品牌信息
     * @param carBrandId
     * @return
     * @throws MyException
     */
    public CarBrand selectCarBrandById(Integer carBrandId) throws MyException {
        ServiceUtil.checkNull(carBrandId,"汽车品牌编号为空");
        CarBrand carBrand;
                try {
                    carBrand = carBrandMapper.selectByPrimaryKey(carBrandId);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(carBrand,"查询失败");

        return carBrand;
    }

    /**
     * 添加汽车品牌
     * @param carBrand
     * @return
     * @throws MyException
     */
    public Integer InsertCarBrand(CarBrand carBrand) throws MyException {
        ServiceUtil.checkNull(carBrand,"汽车品牌编号为空");
        Integer result;
                try {
                    result = carBrandMapper.insert(carBrand);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(result,"添加失败");

        return result;
    }

    /**
     * 根据汽车品牌Id 删除汽车品牌
     * @param carBrandId
     * @return
     * @throws MyException
     */
    public Integer deleteCarBrand(Integer carBrandId) throws MyException {
        ServiceUtil.checkNull(carBrandId,"汽车品牌编号为空");
        Integer result;
                try {
                    result = carBrandMapper.deleteByPrimaryKey(carBrandId);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(result,"删除失败");

        return result;
    }

    /**
     * 根据汽车品牌Id 修改汽车品牌信息
     * @param carBrandId
     * @return
     * @throws MyException
     */
    public Integer UpdateCarBrand(CarBrand carBrandId) throws MyException {
        ServiceUtil.checkNull(carBrandId,"汽车品牌编号不能为空");
        Integer result;
                try {
                    result = carBrandMapper.updateByPrimaryKey(carBrandId);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new MyException("数据异常");
                }
        ServiceUtil.checkNull(result,"修改失败");

        return  result;
    }
}
