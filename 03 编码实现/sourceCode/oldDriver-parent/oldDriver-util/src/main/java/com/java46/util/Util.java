package com.java46.util;


import com.java46.bean.Order;
import com.java46.bean.SHAencryption;
import com.java46.exception.MyException;

import java.util.ArrayList;
import java.util.HashMap;


public class Util {
    /**
     * 保存图片到服务器上的方法
     * @param url
     * @param fileInfo
     * @throws MyException
     */
//    public static void savePicToServer(String url, FileInfo fileInfo) throws MyException {
//        try {
//            //上传图片到本地方法
//            File File = new File(url, fileInfo.getFileFileName());
//            FileUtils.copyFile(fileInfo.getFile(), File);
//        } catch (Exception e) {
//            throw new MyException("添加图片到["+url+"]失败");
//        }
//    }

    /***********************
     *
     * 加密字符的方法
     *
     ***********************
     */

    public  static  String encryption(String str) throws MyException {
        SHAencryption sha=new SHAencryption();
        if (str==""||"".equals(str)){
            throw new MyException("需要加密的密码不能为空");
        }
        try {
            for (int i = 0; i < 1024 ; i++) {
                str=sha.encryptSHA(str);
            }

        } catch (Exception e) {
            throw new MyException("密码加密失败");
        }
        return str;
    }

    /*********************************
     *
     * 快速抛异常
     *
     * @param message
     * @throws MyException
     * ******************************
     */
    public static void throwMyexception(String message) throws MyException {
        throw new MyException(message);
    }



    /*************************************
     *
     * string状态码转Integer数组
     *
     * @param code
     * @return
     * @throws MyException
     * ************************************
     */
    public static ArrayList<Integer> strToint(String code) throws MyException {
        char[] nums=code.toCharArray();
        ArrayList<Integer> integers=new ArrayList<>();
        if (nums.length!=3){
            Util.throwMyexception("状态码只能为3位");
        }
        try {
            for (char numc : nums){
                integers.add(Integer.parseInt(String.valueOf(numc)));
            }
        }catch (NumberFormatException e){
            Util.throwMyexception("状态码只能为数字");
        }
        return integers;
    }

    /***************************************
     *
     * 把状态码转换为hash表
     *
     * @param statusCode
     * @return
     * @throws MyException
     ****************************************
     */
    public static HashMap codeToHash(String statusCode) throws MyException {
        ArrayList<Integer> ints = Util.strToint(statusCode);
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("orderStatus",ints.get(0));
        hashMap.put("listStatus",ints.get(1));
        hashMap.put("repairStatus",ints.get(2));
        return hashMap;
    }

    /******************************************
     *
     * 自动封装订单类
     *
     * @param orderId
     * @param statusCode
     * @return
     *
     * ****************************************
     */
    public static Order packageOrder(Integer orderId,String statusCode) throws MyException {
        Order order = new Order();
        order.setOrderId(orderId);
        ArrayList<Integer> ints = strToint(statusCode);
        order.setOrderStatus(ints.get(0));
        order.setListStatus(ints.get(1));
        order.setRepairStatus(ints.get(2));

        return order;

    }

    /****************************************
     *
     * 拼装线上图片请求地址(无用版)
     *
     * @return
     * **************************************
     */
    public static String formatImgURl(String url,String imgName){
        return url+imgName;
    }



}
