package com.java46.exception;

public class MyException extends Exception {
    private String  errorMsg ;

    public MyException(String errorMsg){
        super();
        this.errorMsg=errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
