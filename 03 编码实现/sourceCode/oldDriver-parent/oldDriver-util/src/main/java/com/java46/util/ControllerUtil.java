package com.java46.util;

import com.java46.bean.JsonBean;

import java.util.List;

public class ControllerUtil {

    /******************************
     * 设置成功的情况的jsonBean
     *
     * @param successData
     * @return
     * ****************************
     */
    public static JsonBean setSuccess(Object successData){
        int count=1;
        if(successData==null){
            count=0;
        }
        try {
            count=((List)successData).size();
        }catch (Exception e){

        }
        return new JsonBean(0,"",count,successData);
    }


    /******************************
     * 设置失败的情况的jsonBean
     *
     * @param errorMsg
     * @return
     * ****************************
     */
    public static JsonBean setFail(String errorMsg){
        return new JsonBean(1,errorMsg,0,null);
    }
}
