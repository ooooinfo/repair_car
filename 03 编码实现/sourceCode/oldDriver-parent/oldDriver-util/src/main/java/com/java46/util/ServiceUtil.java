package com.java46.util;

import com.java46.exception.MyException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.java46.util.Util.strToint;
public class ServiceUtil {

    /*********************************
     * 检查数据是否为空,为空并抛出自定义错误消息
     * 1.检查string
     * 2.检查int
     * 2.检查obj
     *
     * @param data
     * @param errorMsg
     * @return
     * @throws MyException
     * ***********************************
     */

    public static String checkNull(String data,String errorMsg) throws MyException {
        if(data==null||"".equals(data)){
            throw new MyException(errorMsg);
        }
        return data;
    }

    public static int checkNull(int data,String errorMsg) throws MyException {
        if(data==0){
            throw new MyException(errorMsg);
        }
        return data;
    }

    public static Object checkNull(Object data,String errorMsg) throws MyException {
        if(data==null){
            throw new MyException(errorMsg);
        }
        return data;
    }

    /******************************************
     *  判断一个集合是否为null，再判断数组有没有值
     *
     * @param list
     * @param errorMsg
     * @return
     * @throws MyException
     * ******************************************
     */
    public static List checkListNull(List list,String errorMsg) throws MyException {
        if(list==null){
            throw new MyException(errorMsg);
        }
        if (list.size()==0){
            throw new MyException(errorMsg);
        }
        return list;
    }

    


}
