//判断name的字符长度
$(".Name").blur(function() {
	length($(this).val(), "8", "1", "不能大于8个字符", "不能小于1个字符", $(".lable1"));
})
//判断车牌号格式是否正确
$("input[name=che]").blur(function() {
	$(".label2").text("");
	var vehicleNumber = $(this).val();
	var result = "";
	if(vehicleNumber.length == 7) {
		var express = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$/;
		result = express.test(vehicleNumber);
		if(result == false) {
			$(".lable2").text("*请正确输入车牌号");
		} else {
			$(".lable2").text("");
		}
	} else {
		$(".lable2").text("*请正确输入车牌号");
	}
})
//判断手机号格式是否正确
$("input[name=pone]").blur(function() {
	var phoone = $(this).val();
	$(".lable3").text("");
	if(phoone.length == 11) {
		var pattern = /^1[34578]\d{9}$/;
		var result = pattern.test(phoone);
		if(result == false) {
			$(".lable3").text("*请正确输入手机号");
		} else {
			$(".lable3").text("");
		}
	} else {
		$(".lable3").text("*请正确输入手机号");
	}
})
//判断日期是否为空
$("input[name=date]").blur(function(){
	isNull($(this).val(), "日期不能为空", $(".lable4"))
})
//判断验证码不能为空
$("input[name=yan]").blur(function() {
	isNull($(this).val(), "验证码不能为空", $(".lable5"))
})
/*
 	isNull: 判断是否为空
 	duixiang1：要判断的控件对象
 	msg：消息提示语言
 	duixiang2：消息提示语言放置的位置
 * */
function isNull(value, msg, obj) {
	if(value == "") {
		obj.text("");
		obj.text("*" + msg + "");
	} else {
		obj.text("");
	}
}

/*
 	length：判断字符串的长度
 	duixiang1：要判断的控件对象
 	max：最大值
 	min：最小值
 	msg：大于消息提示语言
 	msg2：小于消息提示语言
 	duixiang2：消息提示语言放置的位置
 	
 * */
function length(value, max, min, msg, msg2, object) {
	if(value == 0){
		object.text("");
		object.text("用户名不能为空");
	}
	if(max < value.length) {
		object.text("");
		object.text("*" + msg + "");
	} else if(value.length < min) {
		object.text("");
		object.text("*" + msg2 + "");
	} else {
		object.text("");
	}
}

//点击获取验证码
var validCode = true;
var yanma;
$("button[name=Send]").click(function() {
	var pho = $("input[name=pone]").val();
	if(pho == ""){
		$(".lable3").text("*请输入手机号");
		return;
	}
	$.ajax({
		type: "post",
		dataType: "json",
		data: {
			phone: pho
		},
		url: "http://www.516code.cn/homePage/homepage/notecontorller.do",
		success: function(msg) {
			var yan = msg.data;
			yanma = yan;
		}
	});
	$(this).attr("disabled", "disabled");  
	var time = 60;    
	var code = $(this);    
	if(validCode) {     
		validCode = false;     
		code.addClass("msgs1"); 
		code.css("background-color","gainsboro");
		var t = setInterval(function() {      
			time--;      
			code.html(time + "秒后点击重新获取"); 
			if(time == 0) {      
				$(code).removeAttr("disabled");   
				clearInterval(t);       
				code.html("点击重新获取");       
				validCode = true;       
				code.removeClass("msgs1");  
				code.css("background-color","#66AFE9");
			}     
		}, 1000)
	}
})
//提交客户预约信息
$("input[type=button]").click(function(){
	var Time = $("input[type=datetime-local]").val();
	var Plate = $("input[name=che]").val();
	var Name = $("input[name=Name]").val();
	var Phone = $("input[name=pone]").val();
	var yan = $("input[name=yan]").val();
	if(Name == ""){
		$(".lable1").text("*名字不能为空");
		return;
	}else if(Plate == ""){
		$(".lable2").text("*车牌号不能为空");
		return;
	}else if(Phone == ""){
		$(".lable3").text("*手机号不能为空");
		return;
	}else if(Time == ""){
		$(".lable4").text("*日期不能为空");
		return;
	}else if(yan == ""){
		$(".lable5").text("*验证码不能为空");
		return;
	}else if(yan != yanma){
		$(".lable5").text("*验证码输入错误");
		return;
	}
	
	$.ajax({
		type: "post",
		dataType: "json",
		data: {
			bespeakTime : Time,
			carPlate : Plate,
			customerName : Name,
			customerPhone : Phone,
			state : "0",
		},
		url: "http://www.516code.cn/homePage/homepage/insertBespeak.do",
		success: function(msg) {
			var yan = msg.data;
			alert(yan);
		}
	});
})
