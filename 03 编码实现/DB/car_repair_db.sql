/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50162
Source Host           : localhost:3306
Source Database       : car_repair_db

Target Server Type    : MYSQL
Target Server Version : 50162
File Encoding         : 65001

Date: 2018-08-25 18:28:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id编号',
  `admin_account` varchar(20) NOT NULL COMMENT '管理员账号',
  `admin_pwd` varchar(32) NOT NULL COMMENT '管理员密码',
  `admin_name` varchar(20) NOT NULL COMMENT '管理员姓名',
  `admin_img` varchar(255) DEFAULT NULL COMMENT '管理员头像图片',
  PRIMARY KEY (`admin_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', '123456', '老大', '老大.jpg');
INSERT INTO `admin` VALUES ('2', 'java46', '123456', '大哥', '大哥.jpg');

-- ----------------------------
-- Table structure for bespeak
-- ----------------------------
DROP TABLE IF EXISTS `bespeak`;
CREATE TABLE `bespeak` (
  `bespeak_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '预定id编号',
  `bespeak_time` varchar(30) NOT NULL COMMENT '预定时间',
  `car_plate` varchar(10) NOT NULL COMMENT '预定车牌号',
  `customer_name` varchar(30) NOT NULL COMMENT '预定客户姓名',
  `customer_phone` varchar(20) NOT NULL COMMENT '预定客户联系方式',
  `state` int(1) NOT NULL DEFAULT '0' COMMENT '预约状态（0表示未开启，1表示已解决，2表示已过期）',
  PRIMARY KEY (`bespeak_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bespeak
-- ----------------------------
INSERT INTO `bespeak` VALUES ('1', '2018-8-30 16:30:02', '渝A·XY123', '梁非凡', '12345678909', '0');
INSERT INTO `bespeak` VALUES ('2', '2018-8-30 14:00:27', '渝B·MN456', '面筋哥', '09876543212', '0');

-- ----------------------------
-- Table structure for car_brand
-- ----------------------------
DROP TABLE IF EXISTS `car_brand`;
CREATE TABLE `car_brand` (
  `car_brand_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '车品牌id 编号',
  `car_brand_name` varchar(30) NOT NULL DEFAULT '' COMMENT '汽车品牌名',
  `car_brand_img` varchar(255) DEFAULT NULL COMMENT '车品牌图片',
  PRIMARY KEY (`car_brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of car_brand
-- ----------------------------
INSERT INTO `car_brand` VALUES ('1', '奔驰', '奔驰.jpg');
INSERT INTO `car_brand` VALUES ('2', '宝马', '宝马.jpg');
INSERT INTO `car_brand` VALUES ('3', '奥迪Q5', '奥迪Q5.jpg');
INSERT INTO `car_brand` VALUES ('4', '奥迪Rs4', '奥迪Rs 4.jpg');
INSERT INTO `car_brand` VALUES ('5', '奔驰S级', '奔驰S级.jpg');
INSERT INTO `car_brand` VALUES ('6', '大众', '大众.jpg');
INSERT INTO `car_brand` VALUES ('7', '福特Musrang', '福特Musrang.jpg');
INSERT INTO `car_brand` VALUES ('8', '捷豹E-PACE', '捷豹E-PACE.jpg');
INSERT INTO `car_brand` VALUES ('9', '兰博基尼', '兰博基尼.jpg');
INSERT INTO `car_brand` VALUES ('10', '奥迪Q5', '奥迪Q5.jpg');
INSERT INTO `car_brand` VALUES ('11', '路虎发现SVX', '路虎发现SVX.jpg');
INSERT INTO `car_brand` VALUES ('12', '保时捷911 GT3', '保时捷911 GT3.jpg');

-- ----------------------------
-- Table structure for fault
-- ----------------------------
DROP TABLE IF EXISTS `fault`;
CREATE TABLE `fault` (
  `fault_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '故障信息id编号',
  `fault_info` varchar(400) NOT NULL COMMENT '故障信息',
  `fault_detail` varchar(400) DEFAULT NULL COMMENT '故障详细',
  `order_id` int(10) NOT NULL COMMENT '订单id（外键）',
  PRIMARY KEY (`fault_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fault
-- ----------------------------
INSERT INTO `fault` VALUES ('1', '空调烂完了', null, '1');
INSERT INTO `fault` VALUES ('2', '轮胎烂完了', null, '1');
INSERT INTO `fault` VALUES ('3', '座椅烂了', '需要更换座椅', '2');
INSERT INTO `fault` VALUES ('4', '窗玻璃碎了', '需要更换玻璃', '2');
INSERT INTO `fault` VALUES ('5', '车表面擦挂', '需要喷漆', '4');
INSERT INTO `fault` VALUES ('6', '玻璃烂了', '更换玻璃', '4');

-- ----------------------------
-- Table structure for leave_word
-- ----------------------------
DROP TABLE IF EXISTS `leave_word`;
CREATE TABLE `leave_word` (
  `leaveId` int(11) NOT NULL AUTO_INCREMENT,
  `leave_Name` varchar(20) NOT NULL,
  `leave_phone` varchar(30) NOT NULL,
  `leave_msg` varchar(500) NOT NULL,
  PRIMARY KEY (`leaveId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of leave_word
-- ----------------------------
INSERT INTO `leave_word` VALUES ('1', '张三', '13078975612', '你们这个服务态度很好');
INSERT INTO `leave_word` VALUES ('2', '李四', '130465798', '服务很到位');
INSERT INTO `leave_word` VALUES ('3', '天天', '130546578945', '非常好的一个4s店');
INSERT INTO `leave_word` VALUES ('4', '的成功TV共同', '14515050312', '财富日出发日出发日常不管他');
INSERT INTO `leave_word` VALUES ('5', '老王', '457852321', '服务贼到位，下次还来');
INSERT INTO `leave_word` VALUES ('6', '老王', '457852321', '服务贼到位，下次还来');
INSERT INTO `leave_word` VALUES ('7', '老王', '457852321', '服务贼到位，下次还来');
INSERT INTO `leave_word` VALUES ('8', '老王', '457852321', '服务贼到位，下次还来');
INSERT INTO `leave_word` VALUES ('9', '老三', '457852321', '服务贼到位，下次还来');
INSERT INTO `leave_word` VALUES ('10', '老三', '457852321', '服务贼到位，下次还来');
INSERT INTO `leave_word` VALUES ('11', '老李', '32154648597512', 'The service thief is in place. Come back next time');
INSERT INTO `leave_word` VALUES ('12', '老李', '32154648597512', 'The service thief is in place. Come back next time');
INSERT INTO `leave_word` VALUES ('13', '老李', '32154648597512', 'The service thief is in place. Come back next time');
INSERT INTO `leave_word` VALUES ('14', '李三', '1304567892', '杀局快乐新年促销');
INSERT INTO `leave_word` VALUES ('15', '王三', '160549567482', '这家老司机4s店不愧是老司机，个个都是大佬级别的');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单id编号',
  `order_time` varchar(30) NOT NULL COMMENT '订单时间',
  `car_plate` varchar(10) NOT NULL COMMENT '车牌号',
  `customer_name` varchar(10) NOT NULL COMMENT '客户姓名',
  `customer_phone` varchar(20) NOT NULL COMMENT '客户联系方式',
  `order_status` int(1) NOT NULL DEFAULT '0' COMMENT '订单状态，（0表示未开启，1表示已开启但未完成，2表示已开启已完成）',
  `list_status` int(1) NOT NULL DEFAULT '0' COMMENT '维修计划状态（ 0 表示未开启，1表示已开启但未填写详细，2表示已开启已填写详细但未确认，3表示已确认）',
  `repair_status` int(1) NOT NULL DEFAULT '0' COMMENT '维修状态（0 表示未开启，1表示已开启但未维修完成，2表示以维修完毕）',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('1', '2018-8-16 15:09:43', '渝A123456', '波澜哥', '123456789', '1', '1', '0');
INSERT INTO `order` VALUES ('2', '2018-8-16 15:10:58', '渝c432561', '王思葱', '66666666666', '1', '2', '0');
INSERT INTO `order` VALUES ('3', '2018-8-16 15:12:53', '渝B213455', '马芸', '11111111111', '1', '0', '0');
INSERT INTO `order` VALUES ('4', '2018-8-16 15:14:19', '渝d123456', '李莲洁', '22222222222', '1', '3', '1');
INSERT INTO `order` VALUES ('5', '2018-08-22T12:00', '粤B12345', 'dsad', '13015678912', '1', '0', '0');
INSERT INTO `order` VALUES ('6', '2018-08-23T10:00', '粤B12345', '张三', '13045678912', '1', '0', '0');
INSERT INTO `order` VALUES ('7', '2018-08-24T15:00', '粤B12345', '老王', '13545678912', '1', '0', '0');
INSERT INTO `order` VALUES ('8', '2018-08-25T16:30', '粤B12345', '李四', '13045678912', '1', '0', '0');
INSERT INTO `order` VALUES ('9', '2018-08-26T20:30', '粤B12345', '老李', '13545678912', '1', '0', '0');

-- ----------------------------
-- Table structure for personnel
-- ----------------------------
DROP TABLE IF EXISTS `personnel`;
CREATE TABLE `personnel` (
  `personnel_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工id 编号',
  `personnel_account` varchar(30) NOT NULL COMMENT '员工账号',
  `personnel_pwd` varchar(32) NOT NULL COMMENT '员工密码需要加密',
  `personnel_name` varchar(30) NOT NULL COMMENT '员工姓名',
  `personnel_img` varchar(255) NOT NULL COMMENT '员工头像图片',
  `personnel_type` int(11) NOT NULL COMMENT '员工职位类型(1表示接待人员,2表示维修人员）',
  PRIMARY KEY (`personnel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of personnel
-- ----------------------------
INSERT INTO `personnel` VALUES ('1', 'zhangsan', '123456', '張叁', '张三.jpg', '1');
INSERT INTO `personnel` VALUES ('2', 'lisi', '123456', '李思', '李思.jpg', '2');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品id编号',
  `product_name` varchar(30) NOT NULL COMMENT '产品名',
  `product_num` int(14) unsigned NOT NULL COMMENT '商品数量',
  `product_price` int(10) NOT NULL,
  `product_img` varchar(255) NOT NULL COMMENT '产品图片',
  `product_type_id` int(10) NOT NULL COMMENT '产品类型id',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', '转向系', '50', '200', '转向系.jpg', '1');
INSERT INTO `product` VALUES ('2', '制动系统', '100', '800', '制动系统.jpg', '1');
INSERT INTO `product` VALUES ('3', '行驶系', '60', '200', '行驶系.jpg', '1');
INSERT INTO `product` VALUES ('4', '分动箱', '100', '300', '分动箱.jpg', '1');
INSERT INTO `product` VALUES ('5', '差速锁', '50', '400', '差速锁.jpg', '1');
INSERT INTO `product` VALUES ('6', '底架', '999', '50', '底架.jpg', '1');
INSERT INTO `product` VALUES ('7', '博驰', '500', '300', '发动机1.jpg', '2');
INSERT INTO `product` VALUES ('8', '丰田', '300', '100', '发动机2.jpg', '2');
INSERT INTO `product` VALUES ('9', '奥奔宝', '800', '100', '发动机3.jpg', '2');
INSERT INTO `product` VALUES ('10', '油博士', '600', '50', '发动机4.jpg', '2');
INSERT INTO `product` VALUES ('11', '三威迪棒', '700', '60', '发动机5.jpg', '2');
INSERT INTO `product` VALUES ('12', '酷讯', '550', '30', '发动机6.jpg', '2');
INSERT INTO `product` VALUES ('13', '车载电视', '100', '500', '电子电器1.jpg', '3');
INSERT INTO `product` VALUES ('14', '车载手机充电器', '90', '400', '电子电器2.jpg', '3');
INSERT INTO `product` VALUES ('15', '车载音响', '150', '600', '电子电器3.jpg', '3');
INSERT INTO `product` VALUES ('16', '车载净化器', '50', '300', '电子电器4.jpg', '3');
INSERT INTO `product` VALUES ('17', '车载吸尘器', '40', '500', '电子电器5.jpg', '3');
INSERT INTO `product` VALUES ('18', '汽车充气泵', '60', '3000', '电子电器6.jpg', '3');
INSERT INTO `product` VALUES ('19', '车漆', '200', '300', '保养1.jpg', '4');
INSERT INTO `product` VALUES ('20', '机油', '100', '300', '保养2.jpg', '4');
INSERT INTO `product` VALUES ('21', '除锈喷雾剂', '300', '300', '保养3.jpg', '4');
INSERT INTO `product` VALUES ('22', '划痕蜡', '95', '300', '保养4.jpg', '4');
INSERT INTO `product` VALUES ('23', '护漆膜', '85', '300', '保养5.jpg', '4');
INSERT INTO `product` VALUES ('24', '诺麦柯', '55', '300', '保养6.jpg', '4');

-- ----------------------------
-- Table structure for product_type
-- ----------------------------
DROP TABLE IF EXISTS `product_type`;
CREATE TABLE `product_type` (
  `product_type_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '商品类型id编号',
  `product_type_name` varchar(30) NOT NULL COMMENT '商品类型名',
  PRIMARY KEY (`product_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_type
-- ----------------------------
INSERT INTO `product_type` VALUES ('1', '底盘');
INSERT INTO `product_type` VALUES ('2', '发动机');
INSERT INTO `product_type` VALUES ('3', '电子电器');
INSERT INTO `product_type` VALUES ('4', '维修保养');
